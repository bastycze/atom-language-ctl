#uses "testlib.ctl"

class exampleParentClass
{
  exampleParentClass()
  {
    Debug("Constructor");
  }
};

class exampleChildClass : public exampleParentClass
{
  int memberA;
  const uint CONST_MEMBER = 4;

  protected void doStuff()
  {
    if(true || FALSE)
    {
      memberA = 14;
      memberA += 10;
      for(int i = 0; i <= memberA; ++i)
        continue;

      while(memberA > 0)
        memberA--;

      do
      {
        break;
      } while(true)
    }
  }
};

const string ESC_CHARS = "A string with \"ESC characters\"\n";
const string ESC_CHARS = "A string with unknown ESC \q\s";
