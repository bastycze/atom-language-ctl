# atom-language-ctl

Basic language grammar for WinCC control script, mostly copied from C/C++ grammars.

## Introduction

 * Keywords and data types

## Installation

1. Clone this repo.
2. Make sure you have apm installed.
    - On Mac, you might need to start Atom and go to Atom &gt; Install Shell Commands.
    - On Windows, it's probably already there.
3. Open your terminal, navigate into the repo directory, then run `apm link`.
